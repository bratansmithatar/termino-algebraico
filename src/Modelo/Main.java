/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Util.ExceptionUFPS;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author PERSONAL
 */
public class Main {
    /**
     * 
     * @param args
     * @throws IOException
     * @throws ExceptionUFPS
     * @throws Exception 
     */
     public static void main(String[] args) throws IOException, ExceptionUFPS, Exception {
         /**
          * pido al usuario la cantidad de monomios
          */
       System.out.println("Digite la cantidad de monomios");
       Scanner scan = new Scanner(System.in);
       int n = scan.nextInt();
       /**
        * creo la excepción por si el número digitado es negativo
        */
       if (n<0){
         throw new Exception("Solo se aceptan números positivos");
       }
       /**
        * llamo los métodos y los imprimo
        */
       Polinomio p = new Polinomio(n);
       p.crearPolinomio();
       System.out.println(p.llenarPolinomio());
       System.out.println("valor de la derivada");
       System.out.println(p.getDerivada());
    }
}
