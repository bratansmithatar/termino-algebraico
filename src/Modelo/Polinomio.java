/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Util.ExceptionUFPS;
import java.io.IOException;
import java.util.Scanner;
/**
 *
 * @author PERSONAL
 * creación clase polinomio
 */
public class Polinomio {

    int n;
    private TerminoAlgebraico tamaño[];
    TerminoAlgebraico teral;

    public Polinomio(int n) {
        this.n = n;
        this.tamaño = new TerminoAlgebraico[n];
    }  
     
    /**
     * 
     * @return
     * @throws IOException
     * @throws ExceptionUFPS 
     * Método crear polinomios usando la clase terminoAlgebraico
     */
    public String crearPolinomio() throws IOException, ExceptionUFPS {
        int aux = 0;
        while (aux < n) {
            System.out.println("Digite el exponente");
            Scanner scan = new Scanner(System.in);
            int e = scan.nextInt();

            System.out.println("Digite el valor del monomio");
            float v = scan.nextFloat();

            teral = new TerminoAlgebraico();
            teral.setExponente(e);
            teral.setLiteral('x');
            teral.setValor(v);
            tamaño[aux] = teral;
            aux++;
        }
        return tamaño.toString();
    }
    
    /**
     * método que retorna un string con el polinomio creado en crearPolinomio()
     */
    public String llenarPolinomio() {
        String aux = "";
        for (int j = 0; j < tamaño.length; j++) {
            
            if (j != tamaño.length-1){
                aux += "(" + tamaño[j].getValor() + "" + tamaño[j].getLiteral() + "^" + tamaño[j].getExponente() + ")"+"+";
            }
            else{
                aux += "(" + tamaño[j].getValor() + "" + tamaño[j].getLiteral() + "^" + tamaño[j].getExponente() + ")";
            }
        }
        return aux;
    }
    
    /**
     * método getDerivada que retorna un string con el polinomio derivado
     */
    public String getDerivada()
    {   
        String aux1 = "";
            for (int j = 0; j < tamaño.length; j++) {
                int expDer = tamaño[j].getExponente()-1;
                if (tamaño[j].getExponente() == 1){
                    aux1 += "+(" + tamaño[j].getValor()*0 + "" + tamaño[j].getLiteral() + "^" + 0 + ")";
                    break;
                }
                if (j != tamaño.length-1){
                    aux1 += "(" + tamaño[j].getValor()*tamaño[j].getExponente() + "" + tamaño[j].getLiteral() + "^" + expDer + ")"+"+";
                }
                else{
                    aux1 += "(" + tamaño[j].getValor()*tamaño[j].getExponente() + "" + tamaño[j].getLiteral() + "^" + expDer + ")";
                }
            }
            return aux1;
    }   
}
