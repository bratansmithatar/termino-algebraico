/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Util.ExceptionUFPS;
import java.io.IOException;
import static org.testng.Assert.*;
import org.testng.annotations.Test;

/**
 *
 * @author brayan
 */
public class PolinomioNGTest {
    
    private Polinomio polinomio_prueba;
    
    public PolinomioNGTest() {
    }

    // escenario de prueba:
    
    public void escenario() throws IOException, ExceptionUFPS{
        this.polinomio_prueba = new Polinomio(3);
        polinomio_prueba.crearPolinomio();
        String y = polinomio_prueba.llenarPolinomio();
        
    }

    /**
     * Para este test se compara la derivada creada del polinomio_prueba con la derivada_ideal 
     */
    @Test
    public void testGetDerivada() throws IOException, ExceptionUFPS {
        System.out.println("getDerivada");
        this.escenario();
        String z = polinomio_prueba.getDerivada();
        Polinomio dev = new Polinomio(3);
        dev.crearPolinomio();
        String derivada_ideal = dev.llenarPolinomio();
        assertEquals(z,derivada_ideal);
    }  
}
